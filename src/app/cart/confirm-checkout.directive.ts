import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {CartService} from '../service/cart.service';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

@Directive({ selector: '[appConfirmCheckout]' })
export class ConfirmCheckoutDirective {
  private quantity$: Observable<number> = of();
  private overallQuantity = 0;

  @HostListener('mouseenter') onMouseEnter() {
    if (this.overallQuantity === 0) {
      this.highlight('red');
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }

  @HostListener('click', ['$event.target']) onClick(e: MouseEvent) {
    if (this.overallQuantity !== 0) {
      this.cartService.setClearOrder(true);
    }
  }

  constructor(private el: ElementRef, public cartService: CartService) {
      this.quantity$ = this.cartService.getQuantityOfItems();
      this.quantity$.subscribe(quantity => {
      this.overallQuantity = quantity;
    });
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
