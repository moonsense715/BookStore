import { Component, OnInit } from '@angular/core';
import {AuthService} from '../service/auth.service';
import {CartService} from '../service/cart.service';
import {CartItemModel} from '../model/cart-item-model';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit {
  public shoppingCartItems$: Observable<CartItemModel[]> = of([]);
  public shoppingCartItems: CartItemModel[] = [];
  private tempTotalNumOfItems = 0;
  private tempTotalCost = 0;
  constructor(public authService: AuthService, public cartService: CartService, private router: Router) {
    this.shoppingCartItems$ = this.cartService.getCart();

    this.shoppingCartItems$.subscribe(books => {
      this.tempTotalNumOfItems = 0;
      this.tempTotalCost = 0;
      this.shoppingCartItems = books;
      /* counts the total cost of current cart items, also the overall quantity of items */
      this.shoppingCartItems.forEach( cartItem => {
        this.tempTotalNumOfItems += cartItem.quantity;
        this.tempTotalCost += cartItem.quantity * cartItem.book.price;
      });

      this.cartService.setTotalNumOfItems(this.tempTotalNumOfItems);
      this.cartService.setTotalCost(this.tempTotalCost);

    });
  }

  ngOnInit() { }
  /* sets the outlet visibility, depending on its state */
  public toggleOutlet() {
    if (window.location.pathname.includes('popup:cart') && this.shoppingCartItems.length > 0) {
      this.router.navigate([{ outlets: { popup: null }}]);
    } else {
      this.router.navigate([{ outlets: { popup: 'cart' }}]);
    }
  }
}
