import { Component, OnInit } from '@angular/core';
import {CartService} from '../../service/cart.service';
import {Router} from '@angular/router';
import {CartItemModel} from '../../model/cart-item-model';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-cart-description',
  templateUrl: './cart-description.component.html',
  styleUrls: ['./cart-description.component.less']
})
export class CartDescriptionComponent implements OnInit {
  private cart$: Observable<CartItemModel[]> = of([]);
  private clearCart$: Observable<boolean> = of();
  public cart: CartItemModel[] = [];
  constructor( public cartService: CartService, public router: Router) {
    this.cart$ = this.cartService.getCart();
    this.clearCart$ = this.cartService.getClearOrder();

    this.cart$.subscribe(books => {
      this.cart = books;
    });
    this.clearCart$.subscribe(clear => {
      if (clear) {
        this.cart = [];
        this.cartService.clearCart();
      }
    });
  }

  ngOnInit() { }

  closePopup() {
    this.router.navigate([{ outlets: { popup: null }}]);
  }

  private increaseQuantity(item: CartItemModel): void {
    item.quantity++;
    this.cartService.setTotalNumOfItems(this.cartService.getTotalNumOfItems() + 1);
    this.cartService.setTotalCost(this.cartService.getTotalCost() +  item.book.price);
  }

  private decreaseQuantity(item: CartItemModel): void {
    if (item.quantity > 0) {
      item.quantity--;
      this.cartService.setTotalNumOfItems(this.cartService.getTotalNumOfItems() - 1);
      this.cartService.setTotalCost(this.cartService.getTotalCost() - item.book.price);
    }
  }
  /* removes item from cart and updates the numbers of total cost and quantity */
  private removeItemFromCart(item: CartItemModel): void {
    const index = this.getIndexOfBook(this.cart, item);
    this.cartService.setTotalNumOfItems(this.cartService.getTotalNumOfItems() - item.quantity);
    this.cartService.setTotalCost(this.cartService.getTotalCost() - (item.quantity * item.book.price));
    this.cart.splice(index, 1);
    if (this.cart.length === 0) {
      this.closePopup();
    }
  }

  private getIndexOfBook(array: CartItemModel[], item: CartItemModel): number {
    let indexOfMatch: number = -1;
    array.filter((object, index) => {
      if (object.book === item.book) {
        indexOfMatch = index;
      }
    });
    return indexOfMatch;
  }
}
