import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { CartDescriptionComponent } from './cart-description/cart-description.component';
import { CartService } from '../service/cart.service';
import {ConfirmCheckoutDirective} from "./confirm-checkout.directive";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
      CartComponent,
      CartDescriptionComponent,
      ConfirmCheckoutDirective
  ],
  exports: [
        CartComponent,
        CartDescriptionComponent,
        ConfirmCheckoutDirective
    ]
})
export class CartModule { }
