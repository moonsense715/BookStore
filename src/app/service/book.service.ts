import { Injectable } from '@angular/core';
import { BookModel } from '../model/book-model';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/**
 * Service responsible for all the books in the store
 */
@Injectable()
export class BookService {

  constructor(private http: Http) { }

  // Get books from backend
  getBooks(): Promise<BookModel[]> {
    return this.http.get('https://secure.w3dhub.com/apis/bookstore/1/get-books')
        .map(res => res.json())
        .toPromise();
  }

  getBook(id: number) {
    return this.getBooks()
        .then(books => books.find(book => book.id === id));
  }

  // "Generate" average price by looking at all the books
  getAveragePrice() {
    let total = 0;

    return this.getBooks().then(books => {
      books.forEach(book => total += book.price);
      return total / books.length;
    });
  }

  // Generate categories by getting them from the whole book list
  getCategories() {
    const categories: string[] = [];

    this.getBooks().then(books => {
      books.forEach(book => {
        if (categories.indexOf(book.category) === -1) {
          categories.push(book.category);
        }
      });
    });

    return categories;
  }

}
