import { Injectable } from '@angular/core';
import {BookModel} from '../model/book-model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {CartItemModel} from '../model/cart-item-model';

@Injectable()
export class CartService {
  private itemsInCartSubject: BehaviorSubject<CartItemModel[]> = new BehaviorSubject([]);
  private quantityOfItemsSubject: BehaviorSubject<number> = new BehaviorSubject(0);
  private clearCartSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private quantityOfItems = 0;
  private totalNumOfItems = 0;
  private totalCost = 0;
  private doClearCart: boolean;
  private itemsInCart: CartItemModel[] = [];
  constructor() {
    this.doClearCart = false;
    this.itemsInCartSubject.subscribe(book => this.itemsInCart = book);
    this.quantityOfItemsSubject.subscribe(quantity => this.quantityOfItems = quantity);
    this.clearCartSubject.subscribe(clear => this.doClearCart = clear);
  }

  public getCart(): Observable<CartItemModel[]> {
    return this.itemsInCartSubject.asObservable();
  }

  public getQuantityOfItems(): Observable<number> {
    return this.quantityOfItemsSubject.asObservable();
  }

  public getClearOrder(): Observable<boolean> {
    return this.clearCartSubject.asObservable();
  }

  public setClearOrder(order: boolean): void {
    this.clearCartSubject.next(order);
  }

  /* clear cart and set default values of other attributes at successful checkout */
  public clearCart(): void {
    this.setTotalNumOfItems(0);
    this.setTotalCost(0);
    this.itemsInCart = [];
    this.setClearOrder(false);
  }

  public getTotalNumOfItems(): number {
    return this.totalNumOfItems;
  }

  public setTotalNumOfItems(quantity): void {
    this.totalNumOfItems = quantity;
    this.quantityOfItemsSubject.next(quantity);
  }

  public getTotalCost(): number {
    return this.totalCost;
  }

  public setTotalCost(value: number): void {
    this.totalCost = value;
  }

  /* Add new item to cart. If it is already in cart, then it will just increase its quantity */
  public addItem(book: BookModel): void {
    const params = [...this.itemsInCart];
    if (!this.contains(this.itemsInCart, book)) {
      params.push({'book': book, 'quantity': 1});
    } else {
      const index = this.getIndexOfBook(this.itemsInCart, book);
      this.itemsInCart[index].quantity += 1;
    }
    this.itemsInCartSubject.next(params);
  }
  /* Gets the desired book's index for increasing the quantity */
  private getIndexOfBook(array: CartItemModel[], book: BookModel): number {
    let indexOfMatch: number = -1;
    array.filter((object, index) => {
      if (object.book.price === book.price && object.book.title === book.title && object.book.author === book.author) {
        indexOfMatch = index;
      }
    });
    return indexOfMatch;
  }

  /* checks if the obj is in array or not */
  private contains(array: CartItemModel[], obj: BookModel): boolean {
    for (let i = 0; i < array.length; i++) {
      if (array[i].book.price === obj.price && array[i].book.title === obj.title && array[i].book.author === obj.author) {
        return true;
      }
    }
    return false;
  }

}
