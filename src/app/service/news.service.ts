import { Injectable } from '@angular/core';
import { NewsModel } from '../model/news-model';

/**
 * Service responsible for supporting and handling news
 */
@Injectable()
export class NewsService {

  constructor() { }

  // Return an array of news
  getNews(): NewsModel[] {
    return [
      {
        title: 'New books arrived!',
        text: 'With a book in your hand, you can have the pleasure of breathing in the fresh smell of newly ' +
        'printed books, unwinding yourself by taking an unprecedented journey with finely crafted characters of the ' +
        'novel, laughing and crying with them. So, it is time to curl up on your sofa with book in your hand and lose ' +
        'yourself completely in the charm of new words and storyline of the novel. To bring you this kind of an ' +
        'exquisite experience, new release books sketches creative & entertaining world of words.',
        date: new Date('2017.04.18'),
        url: 'assets/images/news_books.jpg'
      },
      {
        title: 'Redeem your coupons now!',
        text: 'All books can be bought with a HUGE discount! Save big on these exclusive, limited time offers from ' +
        'independent booksellers! You won\'t find offerings like this anywhere but here!',
        date: new Date('2017.04.13'),
        url: 'assets/images/news_discount.jpg'
      },
      {
        title: 'We are open!',
        text: 'We have compiled an extensive selection of alternative education publications, ebooks, DVDs, and ' +
        'audio books by some of the most influential names in the field.',
        date: new Date('2017.04.11'),
        url: 'assets/images/news_open.jpg'
      },
    ];
  }

}
