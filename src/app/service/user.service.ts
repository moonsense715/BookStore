import { Injectable } from '@angular/core';
import { UserModel } from '../model/user-model';
import { LoginResponse } from '../network/login-response';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/**
 * Service talking with the backend to handle registering, logging in and resetting users
 */
@Injectable()
export class UserService {

  private headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' });

  constructor(private http: Http) { }

  registerUser(userName: string, fullName: string, password: string) {

    const test_this = { userName: userName, fullName: fullName, password: password };
    const data = 'data=' + JSON.stringify(test_this);

    return this.http.post(
        'https://secure.w3dhub.com/apis/bookstore/1/register-user',
        data,
        { headers: this.headers })
        .map(res => res.json()).toPromise();
  }

  loginUser(userName: string, password: string): Promise<LoginResponse> {

    const test_this = { username: userName, password: password };
    const data = 'data=' + JSON.stringify(test_this);

    return this.http.post(
        'https://secure.w3dhub.com/apis/bookstore/1/user-login',
        data,
        { headers: this.headers })
        .map(res => res.json())
        .toPromise();
  }

  resetUsers() {
    return this.http.get('https://secure.w3dhub.com/apis/bookstore/1/reset-users')
        .map(res => res.json()).toPromise();
  }

}
