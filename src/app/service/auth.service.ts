import { Injectable } from '@angular/core';
import { UserModel } from '../model/user-model';
import { UserService } from './user.service';

/**
 * Service keeping track of a user's login/logout lifecycle
 */
@Injectable()
export class AuthService {

  private loggedIn: boolean;
  private user: UserModel;

  constructor(private userService: UserService) {
    this.loggedIn = false;
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  logIn(inputName: string, password: string) {
    this.userService.loginUser(inputName, password).then(res => {
      if (res.error) {
        console.log('Error logging in');
      } else {
        this.user = res.user;
        this.loggedIn = true;
      }
    });
  }

  logOut() {
    this.loggedIn = false;
  }

  getUser(): UserModel {
    return this.user;
  }

}
