import { TestBed, inject } from '@angular/core/testing';

import { RecentlyViewedBooksService } from './recently-viewed-books.service';

describe('RecentlyViewedBooksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecentlyViewedBooksService]
    });
  });

  it('should ...', inject([RecentlyViewedBooksService], (service: RecentlyViewedBooksService) => {
    expect(service).toBeTruthy();
  }));
});
