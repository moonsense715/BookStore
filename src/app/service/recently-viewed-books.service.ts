import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class RecentlyViewedBooksService {
  private recentlyViewedSubject: BehaviorSubject<number[]> = new BehaviorSubject([]);
  private recentlyViewedBooks: number[] = [];
  constructor() {
    this.recentlyViewedSubject.subscribe(ids => this.recentlyViewedBooks = ids);
  }

  public getRecentlyViewed(): Observable<number[]> {
    return this.recentlyViewedSubject.asObservable();
  }


  /* Adds the selected book id to array */
  public addToRecentlyViewed(id: number): void {
    const params = [...this.recentlyViewedBooks];
    if (params.length === 5) {
      params.shift();
    }
    if (params.indexOf(id) === -1 && params.length < 5) {
      params.push(id);
    }
    this.recentlyViewedSubject.next(params);
  }

}
