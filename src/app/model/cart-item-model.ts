import {BookModel} from './book-model';
/**
 * Class representing a cart item.
 */
export class CartItemModel {
    book: BookModel;
    quantity: number;
}
