/**
 * Class representing a book.
 */
export class BookModel {
    id: number;
    author: string;
    title: string;
    price: number;
    category: string;
}
