/**
 * Interface representing a new.
 */
export interface NewsModel {
    title: string;
    text: string;
    date: Date;
    url: string;
}
