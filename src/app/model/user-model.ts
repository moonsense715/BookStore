/**
 * Class representing a user.
 */
export class UserModel {
    username: string;
    name: string;
}
