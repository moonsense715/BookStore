/**
 * Created by franciska on 2017.05.09..
 */

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { BookListComponent } from '../pages/book-list/book-list.component';
import { BookDetailComponent } from './book-detail/book-detail.component';

/**
 * Handling root and child routes (for viewing specific books)
 */
const booksRoutes: Routes = [
    { path: '', component: BookListComponent },
    { path: ':id', component: BookDetailComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(booksRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class BooksRoutingModule { }