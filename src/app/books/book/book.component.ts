import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { BookModel } from '../../model/book-model';
import { AuthService } from '../../service/auth.service';
import { CartService } from '../../service/cart.service';
import {RecentlyViewedBooksService} from '../../service/recently-viewed-books.service';

/**
 * Component for a book element in the book list
 */
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.less']
})
export class BookComponent implements OnInit, OnDestroy {
  @Input() book: BookModel;
  @Input() average: number;
  discount = 5;
  private selectedId: number;
  private sub: any;

  constructor(
      public authService: AuthService,
      public cartService: CartService,
      private route: ActivatedRoute,
      public router: Router,
      private recentlyViewed: RecentlyViewedBooksService
  ) { }

  ngOnInit() {
    this.sub = this.route.params
        // (+) converts string 'id' to a number
        .subscribe((params: Params) => {
          this.selectedId = +params['id'];
        });
  }

  onSelect(book: BookModel) {
    this.recentlyViewed.addToRecentlyViewed(book.id);
    this.router.navigate(['/books', book.id]);
  }

  isSelected(book: BookModel) {
    return book.id === this.selectedId;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
