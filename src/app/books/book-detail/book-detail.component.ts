import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BookService } from '../../service/book.service';
import { BookModel } from '../../model/book-model';
import { AuthService } from '../../service/auth.service';
import { CartService } from '../../service/cart.service';
import { slideInDownAnimation } from '../../animations/slide-in-down.animation';

/**
 * Component for viewing one specific book in full screen
 */
@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.less'],
  animations: [slideInDownAnimation]
})
export class BookDetailComponent implements OnInit {
  book: BookModel;

  constructor(
      public authService: AuthService,
      public cartService: CartService,
      private bookService: BookService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
    this.route.params
        // (+) converts string 'id' to a number
        .switchMap((params: Params) => this.bookService.getBook(+params['id']))
        .subscribe((book: BookModel) => this.book = book);
  }

  gotoBooks() {
    const bookId = this.book ? this.book.id : null;
    this.router.navigate(['/books', {id: bookId}]);
  }

}
