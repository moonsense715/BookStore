import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

import { BookListComponent } from '../pages/book-list/book-list.component';
import { BookService } from '../service/book.service';
import { BooksRoutingModule } from './books-routing.module';
import { DiscountPipe } from 'app/pipe/discount.pipe';
import { BookComponent } from './book/book.component';
import { BookDetailComponent } from './book-detail/book-detail.component';

/**
 * The books module, lazy loaded
 */
@NgModule({
  imports: [
    CommonModule,
    BooksRoutingModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [
    BookComponent,
    BookDetailComponent,
    BookListComponent,
    DiscountPipe
  ],
  providers: [ BookService ]
})
export class BooksModule { }
