import { UserModel } from '../model/user-model';

/**
 * Interface representing a login response message.
 */
export interface LoginResponse {
    error: string;
    user: UserModel;
}
