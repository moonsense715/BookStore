import { Component } from '@angular/core';
import { NewsService } from '../service/news.service';
import { NewsModel } from '../model/news-model';

/**
 * Component for the carousel slideshow on the main page
 */
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.less'],
  providers: [NewsService]
})
export class CarouselComponent {

  images: NewsModel[];

  constructor(newsService: NewsService) {
    this.images = newsService.getNews();
  }

}
