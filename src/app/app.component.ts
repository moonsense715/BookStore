import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';

/**
 * The root component of the app, with top bar (routing) and a router outlet
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  constructor(public authService: AuthService) { }
}
