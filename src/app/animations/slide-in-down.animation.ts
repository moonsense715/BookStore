import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideInDownAnimation =
    trigger('slideInDownAnimation', [
        state('*',
            style({
                opacity: 1,
                transform: 'translateX(0)'
            })
        ),

        // route 'enter' transition
        transition(':enter', [

            // styles at start of transition
            style({
                opacity: 0,
                transform: 'translateX(-100%)'
            }),

            // animation at end of transition
            animate('1200ms cubic-bezier(0.175, 0.885, 0.32, 1.275)')
        ]),

        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            animate('0.5s ease-in-out', style({
                opacity: 0,
                transform: 'translateY(100%)'
            }))
        ])
    ]);
