import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import 'hammerjs';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './routes';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { CarouselComponent } from './carousel/carousel.component';
import { AuthService } from './service/auth.service';
import { UserService } from './service/user.service';
import { RegisterComponent } from './pages/register/register.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { CartService} from './service/cart.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartModule } from './cart/cart-module';
import {ContactComponent} from './pages/contact/contact.component';
import {RecentlyViewedBooksService} from './service/recently-viewed-books.service';
import {AboutModule} from './pages/about/about-module';
import { ConfirmCheckoutDirective } from './cart/confirm-checkout.directive';

/**
 * The root module of the app
 */
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    MainComponent,
    CarouselComponent,
    RegisterComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    CartModule,
    AboutModule,
    RouterModule.forRoot(ROUTES),
    MaterialModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyADl6uilextTZdoM8OJTSZg9htcd2Qkvhs'
    })
  ],
  providers: [
    AuthService,
    UserService,
    CartService,
    RecentlyViewedBooksService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
