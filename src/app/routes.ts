import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { MainComponent } from './main/main.component';
import { RegisterComponent } from './pages/register/register.component';
import { AboutComponent } from './pages/about/about.component';
import {CartDescriptionComponent} from './cart/cart-description/cart-description.component';
import {ContactComponent} from './pages/contact/contact.component';

/**
 * Routes handler
 */
export const ROUTES: Routes = [
  { path: '', component: MainComponent},
  { path: 'about', component: AboutComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'cart', component: CartDescriptionComponent, outlet: 'popup' },
  { path: 'books', loadChildren: 'app/books/books.module#BooksModule'},
  { path: '**', component: PageNotFoundComponent},

];
