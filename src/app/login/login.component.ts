import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

/**
 * Component for the login/logout in the top bar
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  inputName: string;
  password: string;

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

  // Attempt login and erase the input fields by setting their values empty
  logIn() {
    this.authService.logIn(this.inputName, this.password);
    this.inputName = '';
    this.password = '';
  }

  // Simple logout by the auth service
  logOut() {
    this.authService.logOut();
  }

}
