import { Component, OnInit } from '@angular/core';
import { BookModel } from '../../model/book-model';
import { BookService } from '../../service/book.service';

/**
 * Component for the book list (books) route
 */
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.less']
})
export class BookListComponent implements OnInit {
  books: BookModel[];
  filteredBooks: BookModel[];
  categories: string[];
  selectedCategory: string;
  all = 'All';

  average: number;

  constructor(protected bookService: BookService) {
    this.bookService.getBooks().then(books => {
      this.books = books;
      this.filteredBooks = books;
    });

    this.selectedCategory = 'All';
  }

  ngOnInit() {
    this.bookService.getAveragePrice().then(price =>
      this.average = price
    );

    this.categories = this.bookService.getCategories();
  }

  search(term: string): void {
    this.filteredBooks = this.books.filter(
        item => item.title.toLowerCase().indexOf(term.toLowerCase()) > -1
        || item.author.toLowerCase().indexOf(term.toLowerCase()) > -1
    );
  }

  filter() {
    if (this.selectedCategory === 'All') {
      this.filteredBooks = this.books;
    } else {
      this.filteredBooks = this.books.filter(
          item => item.category === this.selectedCategory
      );
    }
  }
}
