import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreEventsComponent } from './store-events.component';

describe('StoreEventsComponent', () => {
  let component: StoreEventsComponent;
  let fixture: ComponentFixture<StoreEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
