import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-store-events',
  templateUrl: './store-events.component.html',
  styleUrls: ['./store-events.component.less']
})
export class StoreEventsComponent implements OnInit {
  @Input() image: string;
  @Input() title: string;
  @Input() content: string;
  constructor() { }

  ngOnInit() {
  }

}
