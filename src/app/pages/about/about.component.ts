import { Component, OnInit } from '@angular/core';
import { slideInDownAnimation } from '../../animations/slide-in-down.animation';

/**
 * Component for the about route
 */
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.less'],
  animations: [ slideInDownAnimation ]
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
