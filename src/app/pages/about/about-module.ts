import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AboutComponent} from './about.component';
import {StoreEventsComponent} from './store-events/store-events.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AboutComponent,
        StoreEventsComponent
    ],
    exports: [
        AboutComponent,
        StoreEventsComponent
    ]
})
export class AboutModule { }
