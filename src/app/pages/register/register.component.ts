import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';

/**
 * Component for the register route
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

  inputName: string;
  fullName: string;
  password: string;

  constructor(public userService: UserService) { }

  ngOnInit() {
  }

  // Attempt to register user and erase input values
  registerUser() {
    this.userService.registerUser(this.inputName, this.fullName, this.password).then(result => {
      this.inputName = '';
      this.fullName = '';
      this.password = '';
    });
  }

  // Delete new users and restore original user list
  resetUsers() {
    this.userService.resetUsers().then(result => {
      console.log('User list reset');
    });
  }

}
