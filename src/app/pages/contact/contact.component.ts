import { Component, OnInit } from '@angular/core';

/**
 * Component for the contact route
 */
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.less']
})
export class ContactComponent implements OnInit {
  // variables required by google maps
  public lat = 46.2464929;
  public lng = 20.146448;
  public zoom = 18;
  public message = '';
  public name = '';
  public email = '';
  public telephone = '';
  constructor() { }

  ngOnInit() {
  }

  public onSubmit(): boolean {
    this.message = '';
    this.name = '';
    this.email = '';
    this.telephone = '';
    return true;
  }

}
