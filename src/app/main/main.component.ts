import { Component, OnInit } from '@angular/core';
import { NewsService } from '../service/news.service';
import { NewsModel } from '../model/news-model';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {RecentlyViewedBooksService} from '../service/recently-viewed-books.service';
import {BookModel} from '../model/book-model';
import {BookService} from '../service/book.service';
import {AuthService} from '../service/auth.service';
import {Router} from '@angular/router';

/**
 * Component for the main page route
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less'],
  providers: [NewsService, BookService]
})
export class MainComponent implements OnInit {
  private viewedBooks$: Observable<number[]> = of([]);
  private viewedBooks: number[] = [];
  public books: BookModel[] = [];
  news: NewsModel[];

  constructor(newsService: NewsService, public authService: AuthService, private router: Router,
              recentlyViewed: RecentlyViewedBooksService, private bookService: BookService) {
    this.news = newsService.getNews();
    this.viewedBooks$ = recentlyViewed.getRecentlyViewed();

    this.viewedBooks$.subscribe(books => {
      this.viewedBooks = books;
      this.getBook();
    });
  }

  ngOnInit() {

  }

  private getBook(): void {
    this.viewedBooks.map(id => {
      this.bookService.getBook(id).then(value => {
        this.books.push(value);
      });
    });
  }

  // Open one specific book in full screen
  private goToDetails(id: number): void {
    this.router.navigate(['/books', id]);
  }
}
