import { Pipe, PipeTransform } from '@angular/core';
/**
 * Lower the price with the percent value of discount.
 * Usage:
 *  value | discount:percent
 * Example:
 *  {{ 1000 | discount: 25}}
 *  formats to: 750
 */
@Pipe({
  name: 'discount'
})
export class DiscountPipe implements PipeTransform {
  transform(value: number, discount: number): any {
    return value - (value * discount / 100);
  }
}
